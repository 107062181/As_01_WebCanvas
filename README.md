# Software Studio 2020 Spring

###### tags: `Tag(Assignment 01 Web Canvas)`


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Save button                                  | 1~5%     | N         |


## :memo: Description

> My web design have 17 components 
> 5 tools (Geometrics shapes) ex: Circle, rectangle, triangle, brush, and line.
> 12 support tools (Undo, redo, reset,  etc...).
> Below I am gonna give a brief explanation about how I implemented every function.

### Step 1: Js body explanation

- [x] Declaration of used variables.
- [x] Use of DOM to setup the canvas
```javascript=
 document.addEventListener('DOMContentLoaded', setupCanvas);
```
- [x] Class definition to store size data to create rubber and shapes, and mouse position.
```javascript=0
 class ShapeBoundingBox{//redraw as the user moves the mouse}
 class MouseDownPos{//position where clicked}
 class Location{//location of the mouse}
```
- [x] Set up canvas by a function call.
```javascript=26
 function setupCanvas(){ //executed code}
```
- [x] Different function to react to the differents mouse events
```javascript=0
 ReactToMouseMove(e){//executed code};
 ReactToMouseUp(e){//executed code};
 ReactToMouseDown(e){//executed code};
 ReactToMouseOut(e){//executed code};
```
- [x] Declaration of differents function used as support to the differents tools.  

:rocket: 

### Step 2: 5 main tools

###### tags: Brush
- I use the function changTool to obtain the current tool：
```javascript=1
function ChangeTool(toolClicked){...}
``` 
- The current tool selected in this case the brush is passed to the function
```javascript=2
function drawRubberbandShape(loc){...}
``` 
- that will call the function in question or directly execute the codes in is its brackets in this case bellow(brush) the 
```javascript=3
DrawBrush();// function is called
``` 
- At the same time the used tool is being passed  a value is pass by the 
- 
```javascript=4
changeCursor(number);// to change the cursor depending on the tool in use
``` 

- As explained above, the steps are similar for the other remaining four tools
- Circle
```javascript=5
function ChangeTool('Circle')
{
  if(currentTool === 'circle')
     //excute code
}
``` 
- Rectangle
```javascript=6
function ChangeTool('Rect')
{
    if(currentTool === 'circle')
     //excute code
}
``` 
- Triangle
```javascript=7
function ChangeTool('triangle')
{   
     if(currentTool === triangle)
     //excute code

}
``` 
- Line
```javascript=8
function ChangeTool('line')
{
     if(currentTool === 'line')
     //excute code
}
``` 
### Step 2: 12 support tools
> For the other 12 supports tools 7 of them have directly called from the index.html file responding to the event 
```javascript=9
    onclick="//call function"   
```
- To uploading images 
```javascript=10
    onclick="openImage()";
```
- To undo, redo or clear the canvas
```javascript=12
    onclick="undo()";
    onclick="redo()";
    onclick="clearImage()";
```
- To downlaod an image we first need to press the save button 
```javascript=15
    onclick="saveImage";
```
- and then press download where the download function will be executed.

- The color selection operates under an 
```htmlmixed=1
    <input type="color"...> 
```
- The brush size operates under an
```htmlmixed=1
    <input type="range"...> 
```
- The font-family and the font-size operate under two 
```htmlmixed=
    <select>
        <option>(number)</option>
    </select>
```
- and 
```htmlmixed=
    <select>
       <option> value="\" style="\" </option>
    </select>
```
- that respond to the event 
```javascript=
   document.onkeydown = function(event){
    //execute code
    }
```
- where all has been previously set up in the canvas
```javascript=26
   function setupCanvas(){ //executed code}
```
 




:::info
:bulb: **Remarks:** Save button is the creative tool used in my design.As for me, I found this fully responding to client satisfaction who intend to use my app.

- Here is a demo on how to operate the design
<br>
![permalink](assignment_one.gif)

:::

:::info
:pushpin: Gitlab Page link ➜ [https://107062181.gitlab.io/As_01_WebCanvas ](https://107062181.gitlab.io/As_01_WebCanvas ) 
:::

---

- Summary
[ToC]

> Leave in-line comments! [color=#3b75c6]
> Put your cursor right behind an empty bracket {} and see all your choices.

