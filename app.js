let canvas;
// Context provides functions used for drawing and 
// working with Canvas
let ctx;
// Stores previously drawn image data to restore after
// new drawings are added
let savedImageData;
// Stores whether I'm currently dragging the mouse
let dragging = false;
let strokeColor ;
let fillColor = 'black';
let line_Width ;
let lineCap = 'round';
let lineJoin = 'round';
let strokeColor2 = "white";

let cPushArr = new Array();

let cstep = -1;


// Tool currently using
let currentTool = '';
let canvasWidth = 600;
let canvasHeight = 400;

// Stores whether I'm currently using brush
let usingBrush = false;
// Stores line x & ys used to make brush lines
let brushXPoints = new Array();
let brushYPoints = new Array();
// Stores whether mouse is down
let brushDownPos = new Array();
 
let posX, posY;
let flag = 0;
// Stores size data used to create rubber band shapes
// that will redraw as the user moves the mouse
class ShapeBoundingBox{
    constructor(left, top, width, height) {
        this.left = left;
        this.top = top;
        this.width = width;
        this.height = height;
    }
}
 
// Holds x & y position where clicked
class MouseDownPos{
    constructor(x,y) {
        this.x = x,
        this.y = y;
    }
}
 
// Holds x & y location of the mouse
class Location{
    constructor(x,y) {
        this.x = x,
        this.y = y;
    }
}
 


// Stores top left x & y and size of rubber band box 
let shapeBoundingBox = new ShapeBoundingBox(0,0,0,0);
// Holds x & y position where clicked
let mousedown = new MouseDownPos(0,0);
// Holds x & y location of the mouse
let loc = new Location(0,0);
// Call for our function to execute when page is loaded
document.addEventListener('DOMContentLoaded', setupCanvas);

document.onkeydown = function(event) {
    var key_code = event.keyCode;
    loc = GetMousePosition(posX, posY);
    if(key_code == 13 && currentTool == "text") {
        var String = document.getElementById("virtualText").value;
        var Size = document.getElementById("font-size").value;
        var fontType = document.getElementById("font").value;
        ctx.font =  Size + "px " + fontType;
        ctx.fillStyle = strokeColor.value;
        ctx.strokeStyle = strokeColor.value;
        ctx.fillText(String, loc.x, loc.y, 1000);
        $("#virtualText").css({
            top: -300,
            left: -300
        });
        flag = 1;
        document.getElementById("virtualText").value = "";
        Cpush();
        
    } else if(key_code == 27){
        ctx.fillStyle = strokeColor.value;
        ctx.strokeStyle = strokeColor.value;
        $("#virtualText").css({
            top: -300,
            left: -300
        });
        document.getElementById("virtualText").value = "";
    }
}

function setupCanvas(){
    // Get reference to canvas element
    canvas = document.getElementById('my-canvas');

    strokeColor = document.getElementById('color');
    line_Width = document.getElementById('size');
    // Get methods for manipulating the canvas
    ctx = canvas.getContext('2d');

    

    ctx.strokeStyle = strokeColor.value;

    strokeColor.addEventListener('change', changeColor);

    
    ctx.lineWidth = line_Width.value;

    line_Width.addEventListener('mousemove', changeSize);

    ctx.lineCap = lineCap;
    ctx.lineJoin = lineJoin;

    // Execute ReactToMouseDown when the mouse is clicked
    canvas.addEventListener("mousedown", ReactToMouseDown);
    // Execute ReactToMouseMove when the mouse is clicked
    canvas.addEventListener("mousemove", ReactToMouseMove);
    // Execute ReactToMouseUp when the mouse is clicked
    canvas.addEventListener("mouseup", ReactToMouseUp);
    canvas.addEventListener("mouseout", ReactToMouseOut);  

}
 
function ChangeTool(toolClicked){
    document.getElementById("imageLoader").className = "";
    document.getElementById("save").className = "";
    document.getElementById("brush").className = "";
    document.getElementById("line").className = "";
    document.getElementById("rectangle").className = "";
    document.getElementById("circle").className = "";
    document.getElementById("triangle").className = "";
    document.getElementById("eraser").className = "";
    document.getElementById("undo").className = "";
    document.getElementById("redo").className = "";
    document.getElementById("reset").className = "";

    document.getElementById("text").className = "";
    document.getElementById("select-color").className = "";
    // Highlight the last selected tool on toolbar
    document.getElementById(toolClicked).className = "selected";
    // Change current tool used for drawing
    currentTool = toolClicked;
}
// Returns mouse x & y position based on canvas position in page
function GetMousePosition(x,y){
    // Get canvas size and position in web page
    let canvasSizeData = canvas.getBoundingClientRect();
    return { x: (x - canvasSizeData.left) * (canvas.width  / canvasSizeData.width),
        y: (y - canvasSizeData.top)  * (canvas.height / canvasSizeData.height)
      };
}

function Cpush() {
    cstep++;
    if (cstep < cPushArr.length) cPushArr.length = cstep;
    cPushArr.push(canvas.toDataURL());
}
 
function SaveCanvasImage(){
    // Save image
    savedImageData = ctx.getImageData(0,0,canvas.width,canvas.height);
}
 
function RedrawCanvasImage(){
    // Restore image
    ctx.putImageData(savedImageData,0,0);
}
 
function UpdateRubberbandSizeData(loc){
    // Height & width are the difference between were clicked
    // and current mouse position
    shapeBoundingBox.width = Math.abs(loc.x - mousedown.x);
    shapeBoundingBox.height = Math.abs(loc.y - mousedown.y);
 
    // If mouse is below where mouse was clicked originally
    if(loc.x > mousedown.x){
 
        // Store mousedown because it is farthest left
        shapeBoundingBox.left = mousedown.x;
    } else {
 
        // Store mouse location because it is most left
        shapeBoundingBox.left = loc.x;
    }
 
    // If mouse location is below where clicked originally
    if(loc.y > mousedown.y){
 
        // Store mousedown because it is closer to the top
        // of the canvas
        shapeBoundingBox.top = mousedown.y;
    } else {
 
        // Otherwise store mouse position
        shapeBoundingBox.top = loc.y;
    }
}
 
// Returns the angle using x and y
// x = Adjacent Side
// y = Opposite Side
// Tan(Angle) = Opposite / Adjacent
// Angle = ArcTan(Opposite / Adjacent)
function getAngleUsingXAndY(mouselocX, mouselocY){
    let adjacent = mousedown.x - mouselocX;
    let opposite = mousedown.y - mouselocY;
 
    return radiansToDegrees(Math.atan2(opposite, adjacent));
}
 
function radiansToDegrees(rad){
    if(rad < 0){
        // Correct the bottom error by adding the negative
        // angle to 360 to get the correct result around
        // the whole circle
        return (360.0 + (rad * (180 / Math.PI))).toFixed(2);
    } else {
        return (rad * (180 / Math.PI)).toFixed(2);
    }
}
 
// Converts degrees to radians
function degreesToRadians(degrees){
    return degrees * (Math.PI / 180);
}

 
// Called to draw the line
function drawRubberbandShape(loc){
    ctx.strokeStyle = strokeColor.value;
    ctx.fillStyle = fillColor;
    if(currentTool === "brush"){
        // Create paint brush

        ctx.globalCompositeOperation="source-over";
        DrawBrush();


    }else if(currentTool === "eraser")
    {

    	 ctx.globalCompositeOperation="destination-out";
    	 DrawBrush();	


    } else if(currentTool === "line"){
        // Draw Line
        
        ctx.globalCompositeOperation="source-over";
        ctx.beginPath();
        ctx.moveTo(mousedown.x, mousedown.y);
        ctx.lineTo(loc.x, loc.y);
        ctx.stroke();
    } else if(currentTool === "rectangle"){
        // Creates rectangles
        ctx.globalCompositeOperation="source-over";
        ctx.strokeRect(shapeBoundingBox.left, shapeBoundingBox.top, shapeBoundingBox.width, shapeBoundingBox.height);
    } else if(currentTool === "circle"){
        // Create circles
        ctx.globalCompositeOperation="source-over";
        let radius = shapeBoundingBox.width;
        ctx.beginPath();
        ctx.arc(mousedown.x, mousedown.y, radius, 0, Math.PI * 2);
        ctx.stroke();
    } else if(currentTool === "triangle"){
        
        // create triangle
        ctx.globalCompositeOperation="source-over";
        ctx.beginPath();
        ctx.moveTo(loc.x + ((mousedown.x - loc.x))/2, loc.y);
        ctx.lineTo(mousedown.x, mousedown.y);
        ctx.lineTo(mousedown.x - (mousedown.x - loc.x), mousedown.y);
        ctx.closePath();
        ctx.stroke(); 
    } 

    
     
     
    

}
 
function UpdateRubberbandOnMove(loc){
    // Stores changing height, width, x & y position of most 
    // top left point being either the click or mouse location
    UpdateRubberbandSizeData(loc);
 
    // Redraw the shape
    drawRubberbandShape(loc);
}
 
// Store each point as the mouse moves and whether the mouse
// button is currently being dragged
function AddBrushPoint(x, y, mouseDown){
    brushXPoints.push(x);
    brushYPoints.push(y);
    // Store true that mouse is down
    brushDownPos.push(mouseDown);
}
 
// Cycle through all brush points and connect them with lines
function DrawBrush(){
    for(let i = 1; i < brushXPoints.length; i++){
        ctx.beginPath();
 
        // Check if the mouse button was down at this point
        // and if so continue drawing
        if(brushDownPos[i]){
            ctx.moveTo(brushXPoints[i-1], brushYPoints[i-1]);
        } else {
            ctx.moveTo(brushXPoints[i]-1, brushYPoints[i]);
        }
        ctx.lineTo(brushXPoints[i], brushYPoints[i]);
        ctx.closePath();
        ctx.stroke();
    }

}


 
 function changeColor(){

 	ctx.strokeStyle = strokeColor.value;

 }

 function changeSize(){
 	ctx.lineWidth = line_Width.value;
 }

function ReactToMouseDown(e){
    // Change the mouse pointer to a crosshair
   // canvas.style.cursor = "pointer";
    // Store location 
    
    loc = GetMousePosition(e.clientX, e.clientY);
    // Save the current canvas image
    SaveCanvasImage();
    // Store mouse position when clicked
    mousedown.x = loc.x;
    mousedown.y = loc.y;
    // Store that yes the mouse is being held down
    dragging = true;
 
    // Brush will store points in an array
    if(currentTool === 'brush'){
        usingBrush = true;
        
        AddBrushPoint(loc.x, loc.y);


    }
    else if(currentTool === 'eraser'){
        usingBrush = true;
      
        AddBrushPoint(loc.x, loc.y);
        
    } else if(currentTool === 'text') {
    	posX = e.clientX;
    	posY = e.clientY;
        $("#virtualText").css({
            top: posY,
            left: posX
        });
    }

};
 
function ReactToMouseMove(e){
   // canvas.style.cursor = "pointer";
    loc = GetMousePosition(e.clientX, e.clientY);
 	
    // If using brush tool and dragging store each point
    if(currentTool === 'brush' && dragging && usingBrush){
    	
        // Throw away brush drawings that occur outside of the canvas
        if(loc.x > 0 && loc.x < canvasWidth && loc.y > 0 && loc.y < canvasHeight){
          AddBrushPoint(loc.x, loc.y, true);
            
        }

        RedrawCanvasImage();
        DrawBrush();
        
        

    }
    else if(currentTool === 'eraser' && dragging && usingBrush){
    	
        // Throw away brush drawings that occur outside of the canvas
        if(loc.x > 0 && loc.x < canvasWidth && loc.y > 0 && loc.y < canvasHeight){
           
           AddBrushPoint(loc.x, loc.y, true);

        }
        
        RedrawCanvasImage(); 
        DrawBrush();


    }

    else {
        if(dragging && currentTool != 'text'){

            RedrawCanvasImage();
            UpdateRubberbandOnMove(loc);
        }
    }
    
};
 
function ReactToMouseUp(e){
   // canvas.style.cursor = "pointer";
   if(currentTool != "text") {
   		$("#virtualText").css({
                top: -300,
                left: -300
            });
   }
    loc = GetMousePosition(e.clientX, e.clientY);
    RedrawCanvasImage();
    UpdateRubberbandOnMove(loc);
    dragging = false;
    usingBrush = false;
    Cpush();
};

function ReactToMouseOut(e){
    dragging = false;
    usingBrush = false;
};
 
// Saves the image in your default download directory
function SaveImage(){
    // Get a reference to the link element 
    var imageFile = document.getElementById("img-file");
    // Set that you want to download the image when link is clicked
    imageFile.setAttribute('download', 'image.png');
    // Reference the image in canvas for download
    imageFile.setAttribute('href', canvas.toDataURL());
}
 
function OpenImage(){
    
	var imageLoader = document.getElementById('imageLoader');
    imageLoader.addEventListener('change', handleImage, false);

    function handleImage(e){
	    var reader = new FileReader();
	    reader.onload = function(event){
	        var img = new Image();
	        img.onload = function(){
	            canvas.width = img.width;
	            canvas.height = img.height;
	            ctx.drawImage(img,0,0);
	        }
	        img.src = event.target.result;
	    }
	    reader.readAsDataURL(e.target.files[0]);     
	}
}


function changeCursor(number){
	if(number === '1') {
		$("#my-canvas").css("cursor", "url('brush2.png') 5 15, auto");
	}
	if(number === '2') {
		$("#my-canvas").css("cursor", "url('line2.png'), auto");
	}
	if(number === '3') {
		$("#my-canvas").css("cursor", "url('rect.png'), auto");
	}
	
	if(number === '4') {
		$("#my-canvas").css("cursor", "url('circle.png') 15 10, auto");
	}
	if(number === '5') {
		$("#my-canvas").css("cursor", "url('triangle2.png') 4 14, auto");
	}
	if(number === '6') {
		$("#my-canvas").css("cursor", "url('eraser.png'), auto");
	}
	if(number === '7') {
		$("#my-canvas").css("cursor", "url('text.png'), auto");
	}
}

function undo(){
	if (cstep > 0) {
                    cstep--;
                    var canvasPic = new Image();
                    canvasPic.src = cPushArr[cstep];
                    canvasPic.onload = function () {
                        ctx.clearRect(0, 0, canvas.width, canvas.height);
                        ctx.drawImage(canvasPic, 0, 0);
                    }
          }
          else {
                    cstep = -1;
                    ctx.clearRect(0, 0, canvas.width, canvas.height);
                } 
}

function redo(){
	if (cstep < cPushArr.length-1) {
                    cstep++;
                    var canvasPic = new Image();

                    canvasPic.src = cPushArr[cstep];
                    canvasPic.onload = function () {
                        ctx.clearRect(0, 0, canvas.width, canvas.height);
                        ctx.drawImage(canvasPic, 0, 0);
                    }  
           }
}




function clearImage()
{
	if(confirm("Are you sure you want to delete?")) {
		ctx.clearRect(0,0,canvas.width, canvas.height);
		cstep=-1;
	}

}

